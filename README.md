# auto-install-artix

Some automation on [Artix Linux](https://artixlinux.org) distribution intallation on a computer.

I got this idea from Alkusin who did something similar for the [Void Linux](https://alkusin.net/voidlinux/en) ([french version](https://alkusin.net/voidlinux/fr)) distribution.

When I change of computer, or want a clean install of a messed up system, I usually spend a lot of time to configure everything neat, from system to my personal folder (I am a bit paranoiac on cleanness of $HOME directory).

This setup is meant to speed up all that process, especially for systems like ArchLinux and Artix, where everything must be done manually (a scheme I find long but necessary to have my *own perfect setup*).

## How to use it?

The simpliest is to clone the project directly on an external drive (hdd, ssd or stick) and then work from here,
as you will be able to put all your global (system) and personal files on it.

Then copy the template `conf.in` to `conf` and edit it.

```sh
$ cd /media/my-external-drive
$ git clone https://gitlab.com/riribreizh/auto-install-artix.git .
$ cp conf{.in,}
$ $EDITOR conf
```

The `conf` file contains everything needed to install an Artix distribution from
another one (aka a live CD), but you can enrich this with optional scripts and
folders (more below).

## On the edge of installing

Once you have edited the `conf` file and added your files, you are ready to boot on
an Artix LiveCD. The process of getting and running it is out of scope and wont be
explained here. You want to automatize Artix installation because you know Artix :)

Once running, it's a good idea to enable a time server to ensure the system is
correctly synchronized, especially with `pacman-key` which is very sensitive to time.
(although I could make many installs without a time server)

It's also a good idea to change some pacman settings to speed up downloads. I suggest:
* putting near mirror servers at the top of `/etc/pacman.d/mirrorlist`
* uncommenting `ParallelDownloads = 5` in [options] section of `/etc/pacman.conf`
* adding `ILoveCandy` in [options] section of `/etc/pacman.conf`

Then mount your external drive (or even an internal, as long as it's usable on the
live CD), go to the mounted directory and as **root** run the `install-artix `
script:

```sh
cd /media/my-drive
sudo ./install-artix ...options...
```

Options are:
* **-d** to get more information messages (debug) about what is done
* **-f** to force a full overwrite of the target system (even partitions)
* **-k** to keep target filesystems mounted after the install


### Installing in virtualbox

With shared folders, it's possible to use the tool from the host machine. Just add it to an entry of the shared folder, then mount that virtual folder in your Artix live CD:

```sh
# we want to mount in the Public directory of the Artix live user:
# uid and gid options permit to write on the hosted folder
$ mkdir ~/install-artix
$ sudo mount -t vboxsf -o uid=$(id -u),gid=$(id -g) auto-install-artix ~/install-artix
$ cd ~/install-artix
$ sudo ./install-artix ...options...
```

## conf file

It is a bash script that defines a bunch of variables to tell how to install, where
and what.

The first variables are about system config, for both setup and target system.
It's pretty straightforward (just note that the FONT variable must point to an
installed font on the target system. The default provided `ter-916n` comes from
the `terminus-font` package).

Then is the `TARGET_ROOT` which tells where the target system will be mounted.

`PARTTABLE` defines what `label` will be used in **sfdisk** script
to initialize the device partitions table. If empty, an existing
partitions table wont be touched.

Then is the `DEVICES` array containing how the drives will be used,
which filesystem to use for format, and mount point and options.
Each line contains 8 items:
* **device name**, mostly `/dev/sd??` for disks drives
* **partition label**, optional (and then should be set to `-`)
* **partition size**, or `-` if not revelant or should use the remaining space on the device
* **partition type**, the **sfdisk** type to set the partition to, or `-` if that value is irrevelant (in case of btrfs subvolume for
example)
* **filesystem type** which can be for now `ext?`, `btrfs` or
`btrfs.subvol` (for this filesystem sub-volumes)
* **filesystem options** for its creation, knowing that the target
`/etc/fstab` is generated with LABEL rules, so a `-L` option should
be used.
* **mount point**, obviously where it goes in the target system
* **mount options**, added to the **mount** command and in `/etc/fstab`

### system folder

### users folder

