
# system locale config
KEYMAP=fr-pc
SETUPFONT=eurlatgr
FONT=ter-916n
ZONEINFO=Europe/Paris
LOCALE_SHORT=fr_FR
LOCALE=${LOCALE_SHORT}.UTF-8

# where to mount target system (/mnt is usually a good option)
TARGET_ROOT=/mnt

# partitions and boot
# next are used as sfdisk script commands
# PARTTABLE is the partition table type (not set if empty)
PARTTABLE=dos
# DEVICES is the list of partitions to create with the format:
# device-name part-label part-size part-type fs-type fs-options mount-point mount-options
# an empty item must be set to - (like part-size for remaining space on the drive)
DEVICES=(
  # example with btrfs subvolume and a dedicated ext4 home partition
  # device   # label  # size  # p-type  # fs-type     # fs-options  # mount-dir # mount-options
  #/dev/sda1 ROOT     200G    linux     btrfs         '-L ROOT'     /           'noatime'
  #/dev/sda1 .subvol  -       -         btrfs.subvol  -             /subvol     'compress'
  #/dev/sda2 HOME     -       linux     ext4          '-L HOME -m 0 -T big' /home 'noatime'
)

# list of commands to run before creating partitions
PRE_PARTITION_CMDS=(
)

# list of commands to run before creating filesystems
PRE_FILESYSTEM_CMDS=(
)

# list of commands to run after creating filesystems
POST_FILESYSTEM_CMDS=(
)

# list of commands to run after mounting filesystems
POST_MOUNT_CMDS=(
)

# list of commands to run before unmounting filesystems
PRE_UMOUNT_CMDS=(
)

# boot configuration (leave empty to use default)
BOOT_DEVICE=
# boot root partition (leave empty to use the first partition of $DEVICES)
BOOT_ROOT=
# modules to add to initramfs (mkinitcpio.conf)
BOOT_MODULES=
#BOOT_MODULES="nvidia nvidia_modeset nvidia_uvm nvidia_drm"
# hooks to set in initramfs (mkinitcpio.conf)
BOOT_HOOKS=

# used to set /etc/hostname and entries in /etc/hosts
HOST_NAME=
LOCALNET=localnet

# each HOSTS line is like in etc/hosts: IP address, space(s), hostname(s)
# localhost entries are automatically assigned
HOSTS=(
  #'134.56.78.43 somehost somehost.domain.net'
)

# keyserver to use for pacman gpg conf (for added repos)
PACMAN_KEYSERVER='hkp://keyserver.ubuntu.com'

# target pacman options
PACMAN_OPTIONS=(
  ILoveCandy
  Color
  'ParallelDownloads = 5'
  # example when using pipewire, ignoring some packages providers:
  #'IgnorePkg = jack2 pulseaudio lib32-libpulse'
)

# that will enable lib32 repository in pacman
ENABLE_LIB32=1

# universe mirrors are not yet provided by a package, they might change
# set empty to disable universe repository
UNIVERSE_MIRRORS=(
  'https://universe.artixlinux.org/$arch'
  'https://mirror1.artixlinux.org/universe/$arch'
  'https://mirror.pascalpuffke.de/artix-universe/$arch'
  'https://artixlinux.qontinuum.space/artixlinux/universe/os/$arch'
  'https://mirror1.cl.netactuate.com/artix/universe/$arch'
  'https://ftp.crifo.org/artix-universe/'
)

# enable the following archlinux repositories
ARCH_REPOSITORIES=(extra community multilib)

# unofficial repositories to add (https://wiki.archlinux.org/title/unofficial_user_repositories)
# each line has the format: key name url
# example: 'B545E9B7CD906FE3' 'andontie-aur' 'https://aur.andontie.net/$arch'
ADDED_REPOS=(
)

# init system to use (adds the matching SYSPKG_<init system> and PACKAGES_<init system>)
SYSTEM_INIT=dinit

# base system packages, usually should not be modified
SYSPKG="base base-devel linux linux-firmware"
#SYSPKG="base base-devel btrfs-progs e2fsprogs linux"
SYSPKG_runit="runit elogind-runit"
SYSPKG_dinit="dinit elogind-dinit"

# packages to install when in virtualbox
VMPKG="virtualbox-guest-utils xf86-video-vmware"
# packages to install when NOT in virtualbox (example nvidia drivers)
NOVMPKG=""
#NOVMPKG="lib32-libvdpau lib32-nvidia-utils nvidia nvidia-settings nvidia-utils"

# all packages to install for a basic system (change to your convenience)
PACKAGES="bash-completion connman-gtk cpupower dash expac git gtk-engine-murrine gtk-engines gvim htop lib32-pipewire lib32-pipewire-jack maim man-db man-pages numlockx openbox picom pipewire-alsa pipewire-jack pipewire-pulse rofi rxvt-unicode screen slock slop sshfs terminus-font tint2 ttf-dejavu ttf-font-awesome ttf-roboto wpa_supplicant x11-ssh-askpass xclip xdg-user-dirs xdg-utils xorg-mkfontscale xorg-server xorg-xhost xorg-xinit xorg-xrandr yad yay"

# example of packages sets
#PACKAGES_fontsextra="ttf-inconsolata"
PACKAGES_desktop="ffmpegthumbnailer gpicview gsimplecal galculator keepassxc pcmanfm-gtk3"
#PACKAGES_dev="code"
PACKAGES_net="firefox-i18n-fr"
PACKAGES_music="mpc mpd ncmpc"
PACKAGES_image="nitrogen"
#PACKAGES_games="wine wine-gecko wine-mono winetricks"
PACKAGES_data="artix-backgrounds"
#PACKAGES_apps="blender gimp inkscape"

# extra packages for a full configurations
MORE_PACKAGES="$PACKAGES_fontsextra $PACKAGES_desktop $PACKAGES_dev $PACKAGES_net $PACKAGES_music $PACKAGES_image $PACKAGES_games $PACKAGES_data $PACKAGES_apps"

# init system related package
PACKAGES_runit="acpid-runit bluez-runit cronie-runit connman-runit openntpd-runit openssh-runit socklog"
PACKAGES_dinit="acpid-dinit bluez-dinit cronie-dinit connman-dinit metalog-dinit openntpd-dinit openssh-dinit"

# AUR specific packages (if an AUR frontend is installed)
PACKAGES_AUR=""
# AUR frontend to use for installing AUR packages
# (must be in packages list)
# example: yay -S --aur --noconfirm
AUR_FRONTEND=""
# user to switch to for launching the AUR frontend (uses the first from the USERS array if not set)
AUR_USER=""

# runit configuration
ENABLED_SERVICES_runit="connmand openntpd socklog sshd"
# dinit configuration
ENABLED_SERVICES_dinit="acpid connmand cronie metalog ntpd sshd"

# disable some TTYs
UNUSED_TTYS="3 4 5 6"

# if root login is to be enabled
ROOT_PASSWD=
# groups for all users
USERS_GROUPS="users,games,ftp,http,audio,optical,storage,video"
# only {user} is mandatory
# main user must be the first one (without new line)
USERS=(
  # id # name  # display name  # more groups # home  # shell # password
  -    artix   'Artix User'    'wheel,adm'   -       -       artix
)

# comment it out when you are done modifying this file
# this is check in the install script to ensure you looked at it
CONFIG_SETUP_TOBEDONE=1

# vim:set ts=2 sw=2 et ft=sh:
