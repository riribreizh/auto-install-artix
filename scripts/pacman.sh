# enable_pacman_option {conf-file} {option} [{optional args}]
enable_pacman_option () {
  local target=$1; shift 1
  sed "s@^#\?$1.*@$*@" -i "$target"
  grep -q "^$1" "$target" || sed "/^\[options\]$/a$*" -i "$target"
  return 0
}

# enable_pacman_repo {conf-file} {repo-name}
#    [-i {include-file}] [-s {server-url}...] [-k repo-key]
enable_pacman_repo () {
  local -a servers
  local target=$1 repo=$2 include key savedifs
  shift 2

  while [ $# -gt 0 ]; do
    case $1 in
    -i) include="$2";;
    -s) servers+=("$2");;
    -k) key="$2";;
    esac
    shift 2
  done
  savedids=$IFS
  servers="${servers[*]}"
  if [[ -n $key ]] && ! flagfile pacman-key-$repo-$key; then
    pacman-key --recv-key $key || return $?
    pacman-key --lsign-key $key || return $?
    flagfile -s pacman-key-$repo-$key
  fi

  if ! grep -q "^\[$repo\]" $target; then
    cat $target | gawk -v repo="$repo" -v location="$include" -v servers="$servers" '
BEGIN {
  re_repo = "^(#\\s*)?\\[" repo "\\]\\s*$"
  if (servers != "") split(servers, servlines, ";")
}

# found the repo, maybe commented out
$0 ~ re_repo {
  sub(/^#\s*/, "")
  print
  in_repo = 1
  found_repo = 1
  next
}

# if still in the wanted repo and on an "Include =" line, maybe commented out
in_repo && location != "" && /^(#\s*)?Include\s*=/ {
  filename = gensub(/^(#\s*)?Include\s*=\s*([^# ]*)/, "\\2", 1)
  keep_line = 1
  if (filename == location) keep_line = 0
  print "Include = " location
  if (keep_line) print $0
  location = ""
  next
}

# if still in the wanted repo and on a "Server =" line, maybe commented out
in_repo && servers != "" && /^(#\s*)?Server\s*=/ {
  server = gensub(/^(#\s*)?Server\s*=\s*([^# ]*)/, "\\2", 1)
  keep_line = 1
  for (s in servlines) {
    if (servlines[s] == server) {
      keep_line = 0
      print "Server = " servlines[s]
      delete servlines[s]
    }
  }
  if (length(servlines) == 0) {
    servers = ""
    delete servlines
  }
  if (keep_line) print $0
  next
}

# another repo, add all servers that have not been matched yet
in_repo && /^\s*\[.*\]/ {
  in_repo = 0
  remaining_servers()
  print ""
}

# print any line not processed
{ print }

# at the end if still in the wanted repo, add all servers that have not been matched yet
END {
  if (!found_repo) {
    printf("\n[%s]\n", repo)
    if (location != "") print "Include = " location
  }
  if (in_repo || !found_repo) remaining_servers()
}

function remaining_servers() {
  for (s in servlines) print "Server = " servlines[s]
  delete servlines
}
' > $target.temp && mv $target{.temp,} || return $?
  fi
  return 0
}

do_pacman_setup () {
  local o mir repo pkgs

  # specify a keyserver to be able to import keys from user repositories
  if [[ -n $PACMAN_KEYSERVER ]] && ! grep -q "$PACMAN_KEYSERVER" /etc/pacman.d/gnupg/gpg.conf; then
    msgdbg "add pacman keyserver $PACMAN_KEYSERVER"
    echo "keyserver $PACMAN_KEYSERVER" >> /etc/pacman.d/gnupg/gpg.conf || die
  fi

  if [[ ${#PACMAN_OPTIONS[@]} -gt 0 ]]; then
    msgdbg "set pacman options"
    for o in "${PACMAN_OPTIONS[@]}"; do
      msgdbg "- $o"
      enable_pacman_option /etc/pacman.conf $o || die
    done
  fi

  # multilib packages support
  if [[ $ENABLE_LIB32 = 1 ]]; then
    msgdbg "enable [lib32] repo"
    enable_pacman_repo /etc/pacman.conf lib32 -i /etc/pacman.d/mirrorlist || die
  fi

  # universe packages support
  if [[ ${#UNIVERSE_MIRRORS[@]} -gt 0 ]]; then
    msgdbg "enable [universe] repo"
    enable_pacman_repo /etc/pacman.conf universe \
      -i /etc/pacman.d/mirrorlist-universe || die
    for mir in "${UNIVERSE_MIRRORS[@]}"; do
      [[ -f /etc/pacman.d/mirrorlist-universe ]] &&
        ! grep -q "$mir" /etc/pacman.d/mirrorlist-universe &&
        echo "Server = $mir"
    done > /etc/pacman.d/mirrorlist-universe || die
  fi

  # archlinux packages support
  if [[ ${#ARCH_REPOSITORIES[@]} -gt 0 ]] && ! flagfile archlinux-support; then
    msgdbg "refresh pacman gpg keys"
    pacman-key --refresh-keys || die
    pkgs='artix-archlinux-support'
    grep -q multilib <<<${ARCH_REPOSITORIES[*]} && pkgs+=' lib32-artix-archlinux-support'
    msgdbg "install archlinux support packages: $pkgs"
    pacman -Sy --noconfirm $pkgs || die
    msgdbg "add archlinux repos"
    for repo in "${ARCH_REPOSITORIES[@]}"; do
      msgdbg "- $repo"
      enable_pacman_repo /etc/pacman.conf $repo \
        -i /etc/pacman.d/mirrorlist-arch || die
    done
    # that should be done by the archlinux-keyring post install script
    #pacman-key --populate archlinux || die
    flagfile -s archlinux-support
  fi

  # add additional repos
  if [[ ${#ADDED_REPOS[@]} -gt 0 ]]; then
    msgdbg "add extra repos"
    for (( repo=0; repo<${#ADDED_REPOS[@]}; repo+=3 )); do
      msgdbg "- ${ADDED_REPOS[repo+1]}"
      enable_pacman_repo /etc/pacman.conf ${ADDED_REPOS[repo+1]} \
        -s "${ADDED_REPOS[repo+2]}" -k "${ADDED_REPOS[repo]}" || die
    done
  fi

  msgdbg "update pacman database"
  pacman -Sy || die
}
