# die [{optional message to display}]
die () {
  [[ $# -gt 0 ]] && echo "[ERROR] $@" >&2
  exit 1
}

# msgdbg {message}
msgdbg () {
  [[ $DEBUG ]] && [[ $# -gt 0 ]] && echo -e "[DEBUG] $@"
  return 0
}

# delete the flagfile:
#   flagfile -d
# add the value to the flagfile, if not present yet:
#   flagfile -s {value}
# check if flagfile has a specific value (or existence if no value):
#   flagfile {value}
flagfile () {
  file="/.install-artix.stage"
  [[ -f $CHROOT_FLAGFILE ]] || file="$TARGET_ROOT$file"
  case $1 in
  -d)
    rm -f "$file"
    ;;
  -s)
    [[ -f $file ]] || {
      touch $file || die
      chmod 666 $file || die
    }
    [[ $# -eq 2 ]] && grep -F -q "$2" "$file" || echo "$2" >> "$file"
    ;;
  *)
    if [[ $# -eq 1 ]]; then
      grep -F -q "$1" "$file"
    else
      [[ -f $file ]]
    fi
    ;;
  esac
}
