# check if running as root (or with sudo)
if [[ $(id -u) != 0 ]] && [[ $1 != '-configure-user' ]]; then
  echo "started with by $(id -u): $@"
  die "Run this as root (or with sudo), silly"
fi
# TODO test if in artix with artools installed
[[ -f /etc/artix-release ]] || die "Not in an Artix installation environment"

cd "$(dirname $0)" || die
HERE=$PWD
ME="$PWD/$(basename $0)"

TRUE=$(true)
FALSE=$(false)

# defaults
export FORCE_REINSTALL=${FORCE_REINSTALL:-0}
export DEBUG=${DEBUG:-0}
KEEP_MOUNTED=0
CHROOT_FLAGFILE=/.install-artix.in-chroot

while (( $# )); do
  # ensure standard usage of arguments (replace = by spaced argument)
  [[ $1 == --*=* ]] && set -- "${1%%=*}" "${1#*=}" "${@:2}"
  case "$1" in
  # internal
  -chroot|-configure-user) action=$1;;
  # options
  -f|--force)         FORCE_REINSTALL=1;;
  -d|--debug)         DEBUG=1;;
  -k|--keep-mounted)  KEEP_MOUNTED=1;;
  *)                  break;;
  esac
  shift
done
msgdbg "remaining args: $@"

# set the flag indicating we are in a virtualbox VM
lsmod|grep -q vboxguest && {
  export IN_VM=$TRUE
  echo "Running in a virtualbox machine"
}

#msgdbg "pwd: $HERE"
#msgdbg "dir content: $(ls $HERE)"

# predeclare variables in conf
declare KEYMAP SETUPFONT FONT ZONEINFO LOCALE_SHORT LOCALE
declare TARGET_ROOT TARGET_DEVICE
declare -a DEVICES PARTITIONS
declare -a PRE_PARTITION_CMDS PRE_FILESYSTEM_CMDS
declare -a POST_FILESYSTEM_CMDS POST_MOUNT_CMDS POST_UMOUNT_CMDS
declare BOOT_LOADER BOOT_DEVICE BOOT_ROOT BOOT_MODULES BOOT_HOOKS
declare HOST_NAME LOCALNET
declare -a HOSTS

# check configuration files
[[ -r $HERE/conf ]] || die "No configuration file 'conf' in '$HERE'."
source "$HERE/conf" || die "Error while loading the configuration file, check it."
[[ -z $CONFIG_SETUP_TOBEDONE ]] || die "'conf' file has not been enabled by commenting out CONFIG_SETUP_TOBEDONE. Please read correct accordingly."

ip link | grep -q 'state UP' || die "Network does not seem up, and internet is needed."

if [[ $USER = 'root' ]]; then
  [[ -n $KEYMAP ]] && loadkeys $KEYMAP
  [[ -n $SETUPFONT ]] && setfont $SETUPFONT
fi

# always get the last error in a pipeline, not the last command exit code
set -o pipefail
