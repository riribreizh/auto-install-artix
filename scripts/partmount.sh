declare -r PART_COLS=5

find_root_device () {
  local devidx
  # get the index of root partition from config (with / mount point)
  for (( devidx=0; devidx<${#PARTITIONS[@]}; devidx+=$PART_COLS )); do
    if [[ ${PARTITIONS[devidx+3]} = '/' ]]; then
      echo $devidx
      return 0
    fi
  done
}

set_target_device () {
  local devidx=$1
  [[ "$devidx" ]] || die "No / mount point in PARTITIONS (check configuration file)"
  TARGET_DEVICE=${PARTITIONS[devidx]}
}

mount_root () {
  local devidx
  devidx=$(find_root_device)
  set_target_device $devidx

  msgdbg "target partition for / is ${PARTITIONS[devidx]}"
  if ! [[ -b $TARGET_DEVICE ]]; then
    [[ $1 = '-t' ]] && return 1
    die "$TARGET_DEVICE for / is not a block device"
  fi
  if ! mount | grep -F -q $TARGET_DEVICE; then
    msgdbg "$TARGET_DEVICE not mounted, try to mount it"
    opts=$(sed '/^-$/d' <<<${PARTITIONS[devidx+4]})
    [[ $opts ]] && opts="-o $opts"
    mount -v $opts ${PARTITIONS[devidx]} $TARGET_ROOT || die
  fi
  true
}

do_partitions () {
  local devidx opts script device line type mountdir

  # first detect target device
  devidx=$(find_root_device)

  # try to mount root filesystem (to check for flagfile)
  mount_root -t
  # do partitioning if no flag file or forced reinstall
  if [[ $FORCE_REINSTALL -eq 1 ]] || ! flagfile; then
    # unmount if it was successfully mounted before
    if mount | grep -F -q $TARGET_ROOT; then
      msgdbg "unmount $TARGET_ROOT for partitioning"
      umount -R -v $TARGET_ROOT || die
    fi

    # if something to do before creating partitions
    for cmd in ${PRE_PARTITION_CMDS[@]}; do
      eval $cmd || die
    done

    # create partitions
    for script in "${DEVICES[@]}"; do
      script+='write'
      msgdbg "sfdisk script:
$script"
      device=$(awk '/device:/{print $2}' <<<"$script")
      sfdisk <<<"$script" $device || die
    done

    # if something to do before creating filesystems
    for cmd in ${PRE_FILESYSTEM_CMDS[@]}; do
      eval $cmd || die
    done

    # create filesystems
    for (( devidx=0; devidx<${#PARTITIONS[@]}; devidx+=$PART_COLS )); do
      type=${PARTITIONS[devidx+1]}
      opts=${PARTITIONS[devidx+2]}
      case $type in
      btrfs)
        opts+=' -f'
        ;;
      ext*)
        opts+=' -F'
        ;;
      btrfs.subvol)
        mountdir=$(mktemp -d)
        mount -v ${PARTITIONS[devidx]} $mountdir || die
        msgdbg "btrfs subvolume create -p $mountdir/${PARTITIONS[devidx+3]}/${PARTITIONS[devidx+2]}"
        btrfs subvolume create -p $mountdir/${PARTITIONS[devidx+3]}/${PARTITIONS[devidx+2]} || die
        umount -v $mountdir || die
        continue
        ;;
      fat32)
        type=vfat
        ;;
      esac
      [[ $opts = - ]] && opts=''
      msgdbg "mkfs.$type $opts ${PARTITIONS[devidx]}"
      mkfs.$type $opts ${PARTITIONS[devidx]} || die
    done

    # if something to do after creating filesystems
    for cmd in ${POST_FILESYSTEM_CMDS[@]}; do
      eval $cmd || die
    done
  fi
}

do_mounts () {
  local i dev dir opts subvol cmd
  local -r cols=5

  mount_root
  for (( i=0; i<${#PARTITIONS[@]}; i+=$cols )); do
    dev=${PARTITIONS[i]}
    if ! mount | grep -F -q $dev; then
      dir=$TARGET_ROOT${PARTITIONS[i+3]}
      [[ -d $dir ]] || mkdir -p $dir || die
      opts=$(sed '/^-$/d' <<<${PARTITIONS[i+4]})
      if [[ ${PARTITIONS[i+1]} = 'btrfs.subvol' ]]; then
        subvol=${PARTITIONS[i+2]}
        if [[ -n $subvol ]]; then
          [[ -n $opts ]] && opts+=","
          opts+="subvol=$subvol"
        else
          $dir=''
        fi
      fi
      if [[ -n $dir ]]; then
        [[ $opts ]] && opts="-o $opts"
        msgdbg "mount -v $opts $dev $dir"
        mount -v $opts $dev $dir || die
      fi
    fi
  done
  flagfile || flagfile -s partitions || die

  # mount myself to the chroot for execution
  msgdbg "mount myself to target for chroot execution"
  mkdir -p $TARGET_ROOT/.install-artix || die
  mount -v --bind $HERE $TARGET_ROOT/.install-artix || die

  for cmd in ${POST_MOUNT_CMDS[@]}; do
    eval $cmd || die
  done
}

do_umount () {
  local cmd
  if mount | grep -F -q $TARGET_ROOT; then
    for cmd in ${PRE_UMOUNT_CMDS[@]}; do
      eval $cmd || die
    done

    msgdbg "umount -v $TARGET_ROOT/.install-artix"
    umount -v $TARGET_ROOT/.install-artix || die
    rmdir -v $TARGET_ROOT/.install-artix || die
    msgdbg "umount -R -v $TARGET_ROOT"
    umount -R -v $TARGET_ROOT || die
  fi
}