do_basesystem () {
  local pkgs
  local default_bootloader=grub

  flagfile base-system && [[ $FORCE_REINSTALL -ne 1 ]] && return 0

  # prepare packages to install for base system
  pkgs="$SYSPKG"
  case $SYSTEM_INIT in
  runit) pkgs+=" $SYSPKG_runit";;
  dinit) pkgs+=" $SYSPKG_dinit";;
  *) die "Unsupported init system '$SYSTEM_INIT'";;
  esac

  BOOT_LOADER=${BOOT_LOADER:-$default_bootloader}
  case $BOOT_LOADER in
  grub)
    grep -F -q grub <<<$pkgs || pkgs+=' grub'
    ;;
  syslinux)
    grep -F -q syslinux <<<$pkgs || pkgs+=' syslinux'
    # TODO install fptfdisk only if at least one gpt partition table
    grep -F -q gptfdisk <<<$pkgs || pkgs+=' gptfdisk'
    ;;
  esac

  msgdbg "base system packages: $pkgs"

  # install the base system
  basestrap $TARGET_ROOT $pkgs || die

  msgdbg "create target fstab"
  fstabgen -L $TARGET_ROOT >> $TARGET_ROOT/etc/fstab || die
  # remove our mount from fstab
  sed '\@/.install-artix@d' -i /$TARGET_ROOT/etc/fstab

  flagfile -s base-system
}


do_system_setup () {

  # locale
  cd /etc
  if [[ -n $ZONEINFO ]]; then
    msgdbg "set local time to $ZONEINFO"
    ln -sfv ../usr/share/zoneinfo/$ZONEINFO localtime || die
  fi
  msgdbg "synchronize hardware clock"
  hwclock --systohc

  msgdbg "enable en_US locale"
  sed -e 's/^#\(en_US\.UTF-8\)/\1/' -i /etc/locale.gen
  [[ -n $LOCALE_SHORT ]] && sed -e 's/^#\('$LOCALE_SHORT'\)/\1/g' -i /etc/locale.gen
  if [[ -n $LOCALE ]]; then
    msgdbg "set locale to $LOCALE"
    export LANG=$LOCALE
    cat > /etc/locale.conf <<EOF
LANGUAGE=$LOCALE
LC_ALL=$LOCALE
LANG=$LOCALE
LC_COLLATE=$LOCALE
EOF
    [[ $? -eq 0 ]] || die
  fi

  # console
  if [[ -n $SETUPFONT ]]; then
    msgdbg "set current console font to $SETUPFONT"
    setfont -v $SETUPFONT || die
  fi
  if [[ -n $KEYMAP ]] || [[ -n $FONT ]]; then
    msgdbg "configure console"
    echo '' > /etc/vconsole.conf || die
    if [[ -n $KEYMAP ]]; then
      msgdbg "- keymap = $KEYMAP"
      echo "KEYMAP=$KEYMAP" >> /etc/vconsole.conf || die
    fi
    if [[ -n $FONT ]]; then
      msgdbg "- font = $FONT"
      echo "FONT=$FONT" >> /etc/vconsole.conf || die
    fi
  fi

  # hostname
  if [[ -n $HOST_NAME ]]; then
    msgdbg "set hostname to $HOST_NAME"
    echo "$HOST_NAME" > /etc/hostname || die
    if ! grep -q "$HOST_NAME" /etc/hosts; then
      msgdbg "initialize /etc/hosts"
      cat >> /etc/hosts <<EOF
127.0.0.1 localhost localhost.$LOCALNET $HOST_NAME
::1       localhost
EOF
      [[ $? -eq 0 ]] || die
    fi
    for h in "${HOSTS[@]}"; do
      msgdbg "- add host line: $h"
      grep -q "$h" /etc/hosts || echo "$h" >> /etc/hosts || die
    done
  fi
  flagfile -s system-setup
}

do_install_packages () {
  local pkgs

  if ! flagfile install-packages || [[ $FORCE_REINSTALL -eq 1 ]]; then
    pkgs="$PACKAGES"
    case $SYSTEM_INIT in
    runit) pkgs+=" $PACKAGES_runit";;
    dinit) pkgs+=" $PACKAGES_dinit";;
    esac
    if [[ $IN_VM = $TRUE ]]; then
      pkgs+=" $VMPKG"
    else
      pkgs+= " $NOVMPKG"
    fi
    msgdbg "packages: $pkgs"
    pacman -S --noconfirm $pkgs || die
    if [[ -n $MORE_PACKAGES ]]; then
      msgdbg "more packages: $MORE_PACKAGES"
      pacman -S --noconfirm $MORE_PACKAGES || die
    fi
    flagfile -s install-packages || die
  else
    # update only
    pacman -Su --noconfirm || die
  fi
}

do_post_setup () {
  if ! flagfile post-setup || [[ $FORCE_REINSTALL -eq 1 ]]; then
    msgdbg "generate locales"
    locale-gen
    msgdbg "generate dbus machine-id cookie"
    [[ -f /etc/machine-id ]] || dbus-uuidgen > /etc/machine-id

    case $SYSTEM_INIT in
    runit)
      if [[ -n $UNUSED_TTYS ]]; then
        msgdbg "remove unused ttys"
        for t in $UNUSED_TTYS; do
          rm -fv /etc/runit/runsvdir/default/agetty-tty$t
        done
      fi
      if [[ -n $ENABLED_SERVICES_runit ]]; then
        msgdbg "enable services"
        for s in $ENABLED_SERVICES_runit; do
          ln -sfv /etc/runit/sv/$s /etc/runit/runsvdir/default/$s || die
        done
      fi
      ;;
    dinit)
      if [[ -n $NUM_TTYS ]]; then
        msgdbg "enable $NUM_TTYS ttys"
        sed -i 's@^\(ACTIVE_CONSOLES="/dev/tty\[1-\).*\]"$@\1'$NUM_TTYS']"@' /etc/dinit.d/config/console.conf
      fi
      if [[ -n $ENABLED_SERVICES_dinit ]]; then
        msgdbg "enable services"
        cd /etc/dinit.d/boot.d || die
        for s in $ENABLED_SERVICES_dinit; do
          ln -sfv ../$s || die
        done
      fi
      ;;
    esac

    flagfile -s post-setup
  fi
}

do_configure_system () {
  if [[ -d $HERE/system ]] && [[ $(ls $HERE/system | wc -l) -gt 0 ]]; then
    if [[ -z $(command -v rsync) ]]; then
      msgdbg "add rsync package for quality copy"
      pacman -S --noconfirm rsync || die
    fi
    msgdbg "sync files in system for target"
    rsync -rv "$HERE/system/" / || die
    # use .virtualbox extension to keep files/dirs inside a VM
    ( cd "$HERE/system"
      for f in $(find . -name '*.virtualbox'); do
        if [[ $IN_VM = $TRUE ]]; then
          mv -v "/$f" "/$(dirname $f)/$(basename $f .virtualbox)" || die
        else
          rm -vrf "/$f"
        fi
      done
    )
    # use .novm extension to keep files/dirs outside a VM
	  ( cd "$HERE/system"
      for f in $(find . -name '*.novm'); do
        if [[ $IN_VM = $TRUE ]]; then
          rm -vrf "$/$f"
        else
          mv -v "/$f" "/$(dirname $f)/$(basename $f .novm)" || die
        fi
      done
    )
  fi
  if [[ -f $HERE/configure-system ]]; then
    # doing it in a subshell to avoid poluting the current shell
    msgdbg "run configure-system script"
    ( sh $HERE/configure-system ) || die
  fi
  flagfile -s configure-system
}

do_bootloader () {
  local rootdev
  if ! flagfile bootloader || [[ $FORCE_REINSTALL -eq 1 ]]; then
    # for now, only syslinux is supported
    if [[ -z $BOOT_DEVICE ]]; then
      rootdev=$BOOT_ROOT
      if [[ -z $rootdev ]]; then
        set_target_device $(find_root_device)
        rootdev=$TARGET_DEVICE
      fi
      case $BOOT_LOADER in
      grub)
        msgdbg "grub-install --target=i386-pc $rootdev"
        grub-install --target=i386-pc $rootdev || die
        grub-mkconfig -o /boot/grub/grub.cfg || die
        ;;
      syslinux)
        syslinux-install_update -i -a -m || die
        sed 's@\(APPEND root=\)[^ ]*\(.*\)$@\1'$rootdev'\2@g' -i /boot/syslinux/syslinux.cfg
        ;;
      esac
    else
      # TODO: manual install
      true
    fi
    flagfile -s bootloader
  fi
}

do_aur () {
  if ! flagfile packages-aur; then
    if [[ -n "$PACKAGES_AUR" ]] && [[ -n "$AUR_FRONTEND" ]]; then
      [[ -n $AUR_USER ]] || AUR_USER=${USERS[1]}
      msgdbg "$AUR_FRONTEND $PACKAGES_AUR"
      su $AUR_USER -P -c "$AUR_FRONTEND $PACKAGES_AUR" || die

      if [[ -f $HERE/configure-aur ]]; then
        msgdbg "run configure-aur script"
        ( sh $HERE/configure-aur ) || die
      fi
    fi
    flagfile -s packages-aur
  fi
}
