declare -r USER_COLS=7

do_set_users () {
  local i user cmd

  rm -f /.passwds
  touch /.passwds
  for (( i=0; i<${#USERS[@]}; i+=$USER_COLS )); do
    user=${USERS[i+1]}
    if ! grep -q "^$user:" /etc/passwd; then
      msgdbg "set user $user"
      cmd='useradd -m -U'
      [[ ${USERS[i]} != '-' ]] && cmd="$cmd -u ${USERS[i]}"
      [[ ${USERS[i+2]} != '-' ]] && cmd="$cmd -c '${USERS[i+2]}'"
      [[ -n $USERS_GROUPS ]] && cmd="$cmd -G $USERS_GROUPS"
      if [[ ${USERS[i+3]} != '-' ]]; then
        if grep -q -- '-G' <<<$cmd; then
          cmd="$cmd,"
        else
          cmd="$cmd -G"
        fi
        cmd="$cmd${USERS[i+3]}"
      fi
      [[ ${USERS[i+4]} != '-' ]] && cmd="$cmd -d ${USERS[i+4]}"
      [[ ${USERS[i+5]} != '-' ]] && cmd="$cmd -s ${USERS[i+5]}"
      msgdbg "cmd: $cmd $user"
      eval $cmd $user || die
    fi
    if [[ ${USERS[i+6]} != '-' ]] && ! grep -q "^$user"':[^!*]' /etc/shadow; then
      echo "$user:${USERS[i+6]}" >> /.passwds || die
    fi
  done
  # set root password
  if [[ -n $ROOT_PASSWD ]]; then
    grep -q '^root:[^!*]' /etc/shadow || echo "root:$ROOT_PASSWD" >> /.passwds || die
  else
    msgdbg "root password not set"
  fi
  if [[ -s /.passwds ]]; then
    msgdbg "apply passwords"
    [[ $DEBUG -eq 1 ]] && cat /.passwds
    cat /.passwds | chpasswd
  fi
  rm -f /.passwds
  flagfile -s set-users
}

do_configure_users () {
  local i user home
  for (( i=0; i<${#USERS[@]}; i+=$USER_COLS )); do
    user=${USERS[i+1]}
    home=${USERS[i+4]/-//home/$user}
    if [[ -d $HERE/users/$user ]]; then
      msgdbg "copy user content from $HERE/users/$user to $home"
      rsync -v -r -p --usermap=*:$user --groupmap=*:$user "$HERE/users/$user/" $home/ || die
      flagfile -s copy-user-$user
    fi
    if [[ -f $HERE/configure-user-$user ]]; then
      msgdbg "configure user $user"
      su $user -P -c "$HERE/$(basename $0) -configure-user $user" || die
      flagfile -s configure-user-$user
    fi
  done
}

do_configure_user () {
  local user
  [[ -f $CHROOT_FLAGFILE ]] || die "Not launched from $(basename $0)"
  if [[ -z $1 ]] || [[ $1 != $USER ]]; then
    die "stage user for $USER started without a user or with wrong one ($1)"
  fi

  if [[ -r $HERE/configure-user-$USER ]]; then
    msgdbg "- load $USER user profile"
    # enable avoiding certains things during profile load
    # TODO explain this in README.md
    export NO_USER_PROFILE=1
    # set umask from /etc/login.defs (seems not set from a chroot/su'ed login)
    umask $(awk '/^UMASK/{print $2}' /etc/login.defs)
    source /etc/profile || die
    msgdbg "- run configure-user-$USER script"
    source "$HERE/configure-user-$USER" || die
  fi

}
