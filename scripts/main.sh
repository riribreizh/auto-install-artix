
[[ -z $TARGET_ROOT ]] && TARGET_ROOT=/mnt

case $action in
'')
  [[ -f $CHROOT_FLAGFILE ]] && die "In the chroot'ed environment, bug in the chroot command?"
  pacman -Qs artools-base &>/dev/null || die "Not in an Artix installation environment"

  msgdbg "start install"
  do_partitions
  do_mounts
  do_basesystem

  touch $TARGET_ROOT/$CHROOT_FLAGFILE || die
  artix-chroot $TARGET_ROOT /.install-artix/$(basename $ME) -chroot "$@"
  ret=$?
  rm -f $TARGET_ROOT/$CHROOT_FLAGFILE

  [[ $KEEP_MOUNTED -eq 1 ]] || do_umount
  [[ $ret -eq 0 ]] || die
  echo "ALL OK!!!"
  ;;
-chroot)
  msgdbg "entered chroot"
  do_system_setup
  do_pacman_setup
  do_install_packages
  do_post_setup
  do_configure_system
  do_bootloader
  do_set_users
  do_configure_users
  do_aur
  msgdbg "leaving chroot"
  ;;
-configure-user)
  do_configure_user "$@"
  ;;
esac
